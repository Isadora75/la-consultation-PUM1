﻿# Vous pouvez placer le script de votre jeu dans ce fichier.

# Déclarez sous cette ligne les images, avec l'instruction 'image'
# ex: image eileen heureuse = "eileen_heureuse.png"

# Déclarez les personnages utilisés dans le jeu.
define s = Character("vous",color="#e33b17") 
define p = Character("Mme C.",color="#1727e3") 


# Le jeu commence ici
label start:

    $ menu_metierconnu = False
    scene attentecomic1
    with Dissolve(.5)

    "Vous êtes soignante en gynécologie. Mme C. a rendez-vous avec vous à 10h"
    "Vous avez dix minutes de retard sur votre planning quand vous allez la chercher dans la salle d'attente."
    "Vous dites: \"Bonjour\" puis vous vous présentez."
    
    pause.5
    
    scene bureau1
    with Dissolve(.5)
    
    $ points_confiance = 10
    
menu menu_presentation: 
    
    "Mme C. vous a suivie dans votre bureau. Il faut que vous soyez rapide si vous voulez rattrapper votre retard."
    "Quelle sera votre première question?"
    
    "C'est pour quoi ?":
        "Le temps vous manque, la matinée a été longue, autant aller droit au but."
        $ points_confiance -= 5
        jump presentation_mauvaise
        
    
    "Qu'est-ce que je peux faire pour vous?":
        "Vous êtes en retard mais il s'agit d'une nouvelle patiente. Vous décidez de prendre le temps nécessaire à mieux la connaître."
        $ points_confiance += 5
        jump presentation_bonne
        

    
label presentation_mauvaise:
    
        scene bureau2
        $ menu_presentationOK = False
        p "...Euh...La pilule!"
        jump presentation_faite 
        
label presentation_bonne:
    
        scene bureau2
        $ menu_presentationOK = True
        p "J'aimerais changer de contraception."
        jump presentation_faite
        
label presentation_faite:    
    
    if menu_presentationOK: 
        
        p "Je suis serveuse, je travaille la nuit j'aimerais un moyen de contraception compatible avec mes horaires."
        $ menu_metierconnu = True
        $ points_confiance += 5
        jump antecedents_1
        
    else:
    
        "Soucieuse de connaitre ses habitudes de vie, vous demandez à la patiente ce qu'elle fait dans la vie."
        p "Je travaille dans la restauration."
        scene bureau1
        "La patiente vous agace, elle semble sur la défensive, que vous cache-elle?"
        jump remiseenquestion_1

label remiseenquestion_1:
scene bureau1
menu:

                "Peut-être qu'elle est mal à l'aise?":
                    "Vous vous excusez de l'avoir bousculée et vous reprenez avec elle le motif de consultation tranquillement."
                    $ points_confiance += 1
                    jump presentation_bonne 
            
                "Vous n'avez pas de temps à perdre.":
                    "Des antécédents?"
                    $ points_confiance -=3
                    jump mauvais_antecedents 
            
#label choice_2nice:scene bureau2 jump choice2_done /abandonné/
            


       
        
label antecedents_1 : 
    
menu: 
    
        "Vous continuez votre interrogatoire. Quelle sera votre prochaine question?"
        
        "Avez-vous des problèmes de santé dont vous voudriez me parler aujourd'hui.":
            scene bureau2
            p"J'ai souvent des maux de tête, ça compte?"
            jump migraine_1
    
        "Des antécédents?":
            scene bureau2
            p"Quoi?"
            $ menu_atcd = False
            $ points_confiance -=1
            jump mauvais_antecedents
            
label mauvais_antecedents:  
        scene bureau2
        p "Je ne saispas. Aucun je crois."
        s"Des allergies?"
        p"Oui, je suis allergique aux acariens"
        jump tabac         
       
label migraine_1:
scene bureau1
s "Est-ce qu'on vous a déjà parlé de migraines?" 
scene bureau2
p"Oui, mon médecin traitant dit que ce sont des migraines avec aura"
scene bureau1
s"Je le note c'est important. J'ai besoin de savoir si vous n'avez pas de contre-indications en particulier aux traitements hormonaux : diabète, hypercholestérolémie, allergies etc"
p"Non, à part une allergie aux acariens, je n'ai pas d'autres problèmes de santé."
"Après avoir noté ses antécédents dans son dossier, vous reprenez votre interrogatoire."
jump tabac
        
        
label tabac:       
scene bureau1
s"Est-ce que vous fumez?"
if points_confiance >= 10:
    p"Oui, mais pas beaucoup, 5 par jour à peu près."
    jump  prevention_tabac

else: 
 p"Non"
 jump medicaments


label prevention_tabac:
    
menu: 
 
    "Vous essayez d'informer la patiente sur les risques liés au tabac." 

    "Vous savez que même avec 5 cigarettes vous mettes votre vie et celle des autres en danger.": 
        scene bureau2
        p"5 c'est pas beaucoup avant je fumais un paquet par jour."
        $ points_confiance -=3
        jump medicaments
        
    "Est-ce que vous voulez que l'on parle des risques liés au tabac?": 
        scene bureau2 
        p"Non merci je n'ai pas le temps aujourd'hui mais je veux bien qu'on en parle une prochaine fois."
        s"Très bien, continuons"
        $ points_confiance +=3
        jump medicaments
    "Est-ce que vous avez le souhait d'arrêter ou avez déjà essayé d'arrêter?":
        scene bureau2
        p"Non, pas vraiment."
        $ points_confiance +=1
        jump medicaments

    
label medicaments: 
s"Est-ce que vous avez un traitement?"
p"Non je n'ai pas de traitement."

menu: 

    "Cette réponse vous suffit-elle ?"
    "Oui,je pense qu'elle ne prend pas de traitement":
            scene bureau1
            "Vous notez:\"pas de traitement\" lorsque la patiente vous demande \"les somnifères ça compte?\""
            s"Oui, ça compte. Merci de me le dire. vous en prenez souvent?"
            scene bureau2
            p"Non, seulement de temps en temps."
            $ points_confiance +=2
            jump question_frottis

    "Non":
            scene bureau1
            "Vous savez que l'on peut facilement oublier un traitement que l'on ne prend pas tous les jours ou qui n'est pas prescrit."
            s"Est-ce qu'il vous arrive de prendre des médicaments?"
            scene bureau2
            p"Oui, parfois je prends des somnifères à cause de mes horaires de travail."
            $ points_confiance +=4
            jump question_frottis

label question_frottis:

scene bureau1
s"Est-ce que vous avez déjà fait un frottis?"
p"Qu'est ce que c'est?"

scene horloge15h
menu:

    "A ce moment-là vous jetez un coupd'oeil à votre horloge qui indique 15h. Vous êtes en retard. Que faites-vous?"
    "Vous prenez le temps d'expliquer ce qu'est un frottis à la patiente.": 
        "Elle n'est pas responsable de votre retard et le manque de communication ne vous fera pas avancer plus vite."
        #insérer explication sur le frottis(technique, utilité, recommandation de fréquence)
        $ points_confiance +=10
        jump proposition_frottis



    "Un prélèvement du col":
        scene bureau2
        p"Je l'ai fait en début d'année."
        scene bureau1
        s"Vous vous souvenez du résultat?"
        scene bureau2
        p"Le médcin m'a dit que j'avais une infection. J'ai pris des antibiotiques, qui ont bien marché."
        "Vous réalisez que la patiente a confondu avec un prélèvement vaginal."
        "En voulant gagner du temps vous en avez perdu plus. Vous lui donnez les exlications sur ce qu'est un frottis."
        $ points_confiance -=5
        jump proposition_frottis

label proposition_frottis:
scene bureau2
p"Ah oui je vois. On ne m'avait pas expliqué comme ça.Je l'ai fait pendant ma grossesse il y a 5 ans. Tout était normal."

menu:
    
    "Que lui proposez-vous?"
    "Si vous le souhaitez, on peut le faire aujourd'hui ensemble, ou bien vous pouvez le faire plus tard.":
        "La patiente refuse poliment. Elle a bien compris vos explications. Elle préfère le faire un autre jour."
        jump after_speculum

    "Très bien on va le faire aujourd'hui.":
        "La patiente se décompose et se crispe."
        "vous ne lui avez pas laissé la possibilité de refuser le frottis alors qu'elle n'est pas prête." 
        "La patiente est mal à l'aise au moment du déshabillage."
        p"je dois enlever ma culotte?"
        "Vous êtes en train de préparer votre matériel. Vous lui répondez distraitement."
        scene examen1
        s"Oui, bien sûr, sinon je ne peux pas faire le prélèvement."
        "La patiente s'installe sur la table d'examen le jambes fermées."
        "Vous lui demander d'écarter les jambes. Au moment de poser le spéculum, vous n'y arrivez pas"

        jump speculum

    
label speculum:

menu:

    "Quelle est votre réaction?"

    "Vous tentez de la rassurer.": 
        s"Détendez-vous, plus vous serez tendue, plus ça va faire mal."
        s"C'est important de faire cet examen pour votre santé. Vous risquez un cancer si vous ne le faite pas."
        "Alors qu vous vous apprêtez à  de nouveau insérer le speculum vous réalisez que la patiente est mutique, tendue au bord des larmes."
        "Vous décidez de vous arrêter là."
        "Sage décision car vous vous appretiez à commettre un viol sur cette patiente qui n'avait pas du tout consenti à faire ce frottis."
        "La patiente se rhabille. Vous remercie. Et sort de votre bureau précipitamment."
        jump fin_du_jeu
    
    "Vous renoncez": 
        "Vous lâchez le spéculum."
        s"On est pas obligées de le faire aujourd'hui, est-ce que vous voulez qu'on s'arrête là?"
        "Dans votre tête vous vous repassez tous les signes non verbaux de la patiente et réalisez qu'elle avait montré des signes de refus."
        "Vous vous excusez de lui avoir forcé la main, de ne pas avoir recueilli son consentement."
        "Vous lui proposez de prendre rendez vous quand elle sera prête avec la personne de son choix pour le réaliser si elle le souhaite."
        p" J'aimerais quand-même terminer la consultation car je suis venue pour ma contraception."
        $ points_confiance -=5
    
        jump after_speculum
    
label after_speculum: 
"Vous revenez sur la contraception."









  #if points_confiance <= -10: p "Merci. "Je préfère en rester là.""La patiente rentre chez elle et vous met un avis négatif sur Doogle."GAME OVER
  #if menu_metierconnu=false or menu_atcd = False "la patiente rentre chez elle avec la prescrition de pilule que vous lui avez faite. 
  #très vite elle se rend compte que ce n'est pas du tout ce qu'elle souhaitait. 
  #cette pilule va être difficle à prendre avec ses horaires et elle lit dans les contre-indications" migraine avec aura" 
  #ce qu'elle a oublié de vous dire." 
  #Frustrée par votre entretien, elle ne reprend pas tout de suite rendez-vous.
  
  
label fin_du_jeu:
    
scene fondfindujeu

"FIN DU JEU"
"Ce jeu pédagogique réalisé avec le logiciel libre Renpy  par l'équipe de Pour une MEUF est maintenant terminé. Si vous avez l'impression de ne pas avoir réussi à donner les meilleurs soins à cette patiente vous pouvez recommencer et essayer de faire mieux"
return
    
    
            #...the game continues here 

  

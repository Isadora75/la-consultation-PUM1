# La consultation dont vous êtes l'héroïne

## pour collaborer au projet
Vous devez installer le logiciel *git* sur votre ordinateur.
Si vous utilisez windows, ça se passe ici :
https://git-scm.com/download/win

*Gitlab* est une *forge logicielle* bâtie autour de *git*. Elle y rajoute
des outils collaboratifs de discussions et de fusion de code (*merge request*).

Elle permet à chaque développeur de travailler sur sa propre version du code
sur son ordinateur, puis, quand ils considèrent que le moment est venu
de proposer leur modifications aux auteurs initiaux du projet (*proposition
de fusion de code*).

### Vocabulaire git
* dépôt (repository) : un emplacement où le code est stocké et versionné
* clone : création d'un dépôt local (sur votre ordinateur) à partir d'un dépôt
  distant (sur *gitlab* par exemple)
* commit : action d'enregistrer le code dans le dépôt local
* push : action d'envoyer le code du dépôt local vers
  un dépôt distant
* pull : action de récupération du code depuis un dépôt distant vers
  le dépôt locat, puis mise à jour de la *copie de travail*

### Accès au code
Vous pouvez utiliser la fonction de téléchargement de *gitlab*,
ou bien *cloner* le dépôt sur votre ordinateur.

### Processus de travail
Voici les étapes pour proposer des modifications au code :
* Vous créer un compte *gitlab* si vous n'en disposez pas déjà
* *FORK* le projet. Votre version du projet sera disponible
  à l'URL ` https://gitlab.com/<votre pseudo>/la-consultation-PUM1`
* cloner le dépôt sur votre ordinateur
* **conseillé** : vous créer une branche de travail
* faires vos modifications
* *commit*
* *push* (votre branche de travail)
* créer un *merge request* sur *gitlab* pour que vos modifications soient
  intégrées au dépôt original et partagées par tout le monde
